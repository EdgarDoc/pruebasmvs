<!DOCTYPE html>

<html lang= 'es'>
<head>
   <meta charset="UTF-8">
   <link rel="stylesheet" type="text/css" >
   <title>Prueba MVS</title>
<head>
   
<body>
 <!--SECTION Elementos de la barra principal -->
   <header id="barra">
       <div class="orden">
            <div><a class="selec" href="">Link 1</a></div>   
            <div><a class="selec" href="">Link 2</a></div>   
            <div id="headerImg">
                <img id="Img" alt="Logo" src="https://revimex.s3.us-east-2.amazonaws.com/web-seo/media/icons/header/logostatic.png">
            </div>
       </div>
   </header>
<!-- !SECTION -->

<!-- SECTION  Imagen y recuadros derecho e izquierdo -->
   <section id="seccion">
         <div id="Foto">
            <img class="imagen" src="https://revimex.s3.us-east-2.amazonaws.com/web-seo/media/img/home/static_home22.png" alt="back-image-home" id="HomeImg">
         </div>
         <div id="caja">         
                <ul class="lista">
                    <h2 class="subt">PROS</h2>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>

                 </ul>    
                <ul class="lista">
                    <h2 class="subt">CONTRAS</h2>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>
                    <li>Lorem Ipsum has been the industry's standard</li>
                </ul>
         </div>
   </section>
<!-- !SECTION -->

   <footer id="pie"><p>©2019 | MVS</p><p class="fs-8">Todos los derechos reservados</p></footer>

</body>
</html>


<style>
/* SECTION Estilos */
    body
    {
        font-size: 15px;
        margin: auto;
        background-image: linear-gradient(to right top, green,yellow,orange,red,purple,blue);
    }

    div
    {
        display: flex;
        align-items: center;
        justify-content: center;

    }

    .orden
    {
        justify-content: space-between;
    }

    .selec
    {
        text-indent: 20px;
        color: skyblue;
        font-size: 20px;
    }
    .selec:hover
    {
        color: white;
    }
    .subt
    {
        display: flex;
        justify-content: center;
    }
    .lista
    {
        display: flex;
        flex-direction: column;
        flex: 1 1;
        border: 2px solid rgba(0, 0, 0, .92);
        border-radius: 2rem;
        background-color: rgba(255,255,255,0.3);
        color: rgba(0, 0, 0, .92);
        padding-bottom: 15px;
    }

    .fs-8
    {
        font-size: 8px;
    }
    .imagen{
        width: 55vw;
        height: 30vh;
    }

    .imagen:hover
    {
        box-shadow: 1px 1px 4px 2px rgba(0, 0, 0, .92);
    }
    #barra
    {
        background-image: linear-gradient(to top, black, red);
        text-transform: uppercase;

    }


    #seccion
    {
        height: 94vh;
        display: flex;
        flex-direction: column;
        justify-content: center;
    }

    #pie
    {
        text-align: center;
    }

    p{
        font-family: sans-serif;
        font-size: 10px;
    }

    /* NOTE media querie que cambia el logo de lugar */
    @media only screen (max-width: 769px){
       
        .orden{
            flex-direction: row-reverse;
        }

    }
/* !SECTION */
</style>

